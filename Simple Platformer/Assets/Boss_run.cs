﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_run : StateMachineBehaviour
{
    public float speed = 2.5f;
    public float attackRange = 6.6f;

    Transform player;
    Rigidbody2D rb;
    Boss_Flip boss_flip;

    public float timer;
    public float minTime;
    public float maxTime;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateinfo, int layerindex)
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = animator.GetComponent<Rigidbody2D>();
        boss_flip = animator.GetComponent<Boss_Flip>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (timer <= 0)
        {
            animator.SetTrigger("idle");
            animator.SetTrigger("attack2");
            timer = 15;
        }
        else
        {
            timer -= Time.deltaTime;
        }
        boss_flip.LookAtPlayer();

        Vector2 target = new Vector2(player.position.x, rb.position.y);
        Vector2 newPos = Vector2.MoveTowards(rb.position, target, speed * Time.fixedDeltaTime);
        rb.MovePosition(newPos);

        if (Vector2.Distance(player.position, rb.position) <= attackRange)
        {
            // Attack
            animator.SetTrigger("attack");
        }
        //else if (Vector2.Distance(player.position, rb.position) >= attackRange)
        //{
        //    animator.SetTrigger("attack2");
        //}
        else
        {
            animator.SetTrigger("attack2");
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.ResetTrigger("attack");
        //animator.ResetTrigger("attack2");
    }


}
