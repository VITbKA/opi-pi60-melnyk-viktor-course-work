﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    public int health;

    private Animator anim;
    public GameObject bloodEffect;

    public Slider healthBar;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        healthBar.value = health;
    }

    public void TakeDamage(int damage)
    {
        anim.SetTrigger("Hurt");
        if (GetComponent<Enemy>().GetComponent<PlatformPatrol>())
            GetComponent<PlatformPatrol>().WTF();

        health -= damage;
        Debug.Log("Damage TAKEN !");

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        // Die animation
        anim.SetBool("isDead", true);
        Instantiate(bloodEffect, transform.position, Quaternion.identity);

        this.enabled = false;
        Debug.Log("Enemy DIE");
    }
}
