﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //float moveSpeed = 7f;

    //public Rigidbody2D rb;

    //private PlayerController1 player;
    //private Vector2 moveDirection;

    //void Start()
    //{
    //    rb = GetComponent<Rigidbody2D>();
    //    player = GameObject.FindObjectOfType<PlayerController1>();
    //    moveDirection = (player.transform.position - transform.position).normalized * moveSpeed;
    //    rb.velocity = new Vector2(moveDirection.x, moveDirection.y);
    //    Destroy(gameObject, 3f);
    //}

    //void OnTriggerEnter2D (Collider2D other)
    //{
    //    if (other.gameObject.name.Equals("PlayerController1"))
    //    {
    //        Debug.Log("HIT");
    //        Destroy(gameObject);
    //    }
    //}

    public float speed;
    public int damage = 2;

    public GameObject hitEffect;

    private Transform player;
    private Vector2 target;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        target = new Vector2(player.position.x, player.position.y);

        if (GameObject.FindGameObjectWithTag("OMG"))
        {
            FindObjectOfType<AudioManager>().Play("EnemyShootArrow");
        }
            
    }

    void Update()
    {
        //if (CompareTag("OMG"))
            //FindObjectOfType<AudioManager>().Play("EnemyShootArrow");

        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (transform.position.x == target.x && transform.position.y == target.y)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<AudioManager>().Play("PlayerHit");
            // Damage player
            //other.GetComponent<Enemy>().TakeDamage(damage);
            // other.GetComponent<PlayerHealth>().health -= damage;
            other.GetComponent<PlayerHealth>().GetComponent<Animator>();
            other.GetComponent<PlayerHealth>().Hurt(damage);
            Debug.Log(other.GetComponent<PlayerHealth>().health);

            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
            Debug.Log("HIT PLAYER");
            Destroy(gameObject);
        } else
        {
            Destroy(gameObject, 4f);
        }
    }
}
