﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public int health;
    //public float speed = 1f;

    //private float dazedTime;
    //public float startDazedTime = 1.2f;

    private Animator anim;
    public GameObject bloodEffect;

    PlatformPatrol p;
    //public Shake shake;

    public Slider healthBar;

    [SerializeField]
    private GameObject bossOverUI;

    void Start()
    {
        //shake = GameObject.FindGameObjectWithTag("ScreenShake").GetComponent<Shake>();
        anim = GetComponent<Animator>();
        p = GetComponent<PlatformPatrol>();
        //anim.SetBool("isRunning", true);
    }

    void Update()
    {
        if (gameObject.CompareTag("boss")) healthBar.value = health;
        //if (dazedTime <= 0)
        //{
        //    patrolSpeed.speed = 5f;
        //}
        //else
        //{
        //    patrolSpeed.speed = 0;
        //    dazedTime -= Time.deltaTime;
        //}
        //if (patrolSpeed.speed == 0)
        //{
        //    anim.SetBool("isRunning", false);
        //}
        //else
        //{
        //    anim.SetBool("isRunning", true);
        //}
        //transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    void FixedUpdate()
    {
        //вхідні дані про переміщення вліво-вправо
        //anim.SetFloat("move", Mathf.Abs(speed));
    }

    public void TakeDamage(int damage)
    {
        if (CompareTag("ninja"))
        {
            FindObjectOfType<AudioManager>().Play("AttackPlayer");
        }
        if (CompareTag("warrior"))
        {
            FindObjectOfType<AudioManager>().Play("AttackPlayer");
        }
        if (CompareTag("OMG"))
        {
            FindObjectOfType<AudioManager>().Play("AttackPlayer");
        }
        if (CompareTag("boss"))
        {
            FindObjectOfType<AudioManager>().Play("AttackPlayer");
        }

        anim.SetTrigger("Hurt");
        //dazedTime = startDazedTime;
        if (GetComponent<Enemy>().GetComponent<PlatformPatrol>())
            GetComponent<PlatformPatrol>().WTF();
        
        health -= damage;
        Debug.Log("Damage TAKEN !");

        if (health <= 0)
        {
            Die();
        }
    }


    void Die()
    {
        if (gameObject.CompareTag("boss")) healthBar.value = 0;
        //shake.CamShake();

        // Die animation
        anim.SetBool("isDead", true);
        Instantiate(bloodEffect, transform.position, Quaternion.identity);

        this.enabled = false;

        GetComponentInChildren<BoxCollider2D>().enabled = false;
        GetComponent<CapsuleCollider2D>().enabled = false;

        if (CompareTag("ninja"))
        {
            FindObjectOfType<AudioManager>().Close("EnemyAttackWind");
            GetComponent<Enemy>().enabled = false;
            GetComponent<Enemy_attack>().enabled = false;
        }
        if (CompareTag("warrior"))
        {
            FindObjectOfType<AudioManager>().Close("EnemyAttack");
            GetComponent<Enemy>().enabled = false;
            GetComponent<Enemy_attack>().enabled = false;
            GetComponent<PlatformPatrol>().enabled = false;
        }
        if (CompareTag("OMG"))
        {
            FindObjectOfType<AudioManager>().Close("EnemyTeteva");
            GetComponent<Enemy>().enabled = false;
            GetComponent<Enemy_shoot>().enabled = false;
            GetComponent<Enemy_Follow>().enabled = false;
        }

        if (GetComponent<Enemy>().GetComponent<PlatformPatrol>())
        {
            GetComponentInChildren<BoxCollider2D>().enabled = false;
            GetComponent<PlatformPatrol>().enabled = false;
        }

        if (CompareTag("boss"))
        {
            FindObjectOfType<AudioManager>().Close("MainTheme");
            FindObjectOfType<AudioManager>().Play("BossDeath");
            GetComponent<Enemy>().enabled = false;
            GetComponent<Enemy_shoot>().enabled = false;
            GetComponent<CapsuleCollider2D>().enabled = false;
            bossOverUI.SetActive(true);
        }

        Debug.Log("Enemy DIE");
    }
}
