﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_attack : MonoBehaviour
{
    private float timeBtwAttack = 0f;
    public float startTimeBtwAttack = 1f;

    public Transform attackPos;
    public float attackRange = 0.6f;
    public LayerMask whatIsEnemies;

    public int damage = 1;
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Time.time >= timeBtwAttack)
        {
            Attack();
            timeBtwAttack = Time.time + 1f / startTimeBtwAttack;
        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    public void Attack()
    {
        if (CompareTag("ninja"))
            FindObjectOfType<AudioManager>().Play("EnemyAttackWind");
        if (CompareTag("warrior"))
            FindObjectOfType<AudioManager>().Play("EnemyAttack");
        
        // Play an attack animation
        anim.SetTrigger("attack");

        // Detect enemies in range of attack
        Collider2D[] playerToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);

        // Damage them
        foreach (Collider2D p in playerToDamage)
            p.GetComponent<PlayerHealth>().Hurt(damage);

        
    }

    void onDrawGizmosSelected()
    {
        if (attackPos == null)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
