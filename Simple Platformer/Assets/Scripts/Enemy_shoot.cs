﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_shoot : MonoBehaviour
{
    //public GameObject bulletPrefab;

    //float fireRate;
    //float nextFire;

    //void Start()
    //{
    //    fireRate = 1f;
    //    nextFire = Time.deltaTime;
    //}

    //void Update()
    //{
    //    TimeToFire();
    //}

    //void TimeToFire()
    //{
    //    if (Time.deltaTime > nextFire)
    //    {
    //        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
    //        nextFire = Time.deltaTime + fireRate;
    //    }
    //}


    //public int damage = 1;
    //public LayerMask whatIsPlayer;

    private float timeBtwShots;
    public float startTimeBtwShots = 1.2f;
    public float bulletForce = 0f;

    public Transform firePoint;
    public GameObject bulletPrefab;

    public Animator anim;

    void Start()
    {
        timeBtwShots = startTimeBtwShots;
    }

    void Update()
    {
        Shooting();
    }

    public void Shooting()
    {
        if (timeBtwShots <= 0)
        {
            if (CompareTag("OMG"))
            {
                FindObjectOfType<AudioManager>().Play("EnemyTeteva");
                anim.SetTrigger("attack");
            }
            if (CompareTag("boss"))
            {
                FindObjectOfType<AudioManager>().Play("PistolShoot");
                anim.SetTrigger("attack");
            }

            GameObject bullet = Instantiate(bulletPrefab, transform.position, firePoint.rotation/*Quaternion.identity*/);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);

            timeBtwShots = startTimeBtwShots;
        }
        else
        {
            timeBtwShots -= Time.deltaTime;
        }
    }
    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    if (other.CompareTag("Player"))
    //    {
    //        other.GetComponent<PlayerHealth>().health -= damage;
    //        Debug.Log(other.GetComponent<PlayerHealth>().health);
    //        Destroy(gameObject);
    //    }
    //}
}
