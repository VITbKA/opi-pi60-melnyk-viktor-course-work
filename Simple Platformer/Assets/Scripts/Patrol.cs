﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float speed = 2f;
    private float waitTime;
    public float startWaitTime;

    public Transform[] moveSpots;
    private int randomSpot;

    //public Transform moveSpot;
    //public float minX, maxX;
    //public float minY, maxY;

    private Animator anim;
    private float dazedTime;
    //public float startDazedTime = 1.2f;

    void Start()
    {
        waitTime = startWaitTime;

        //moveSpot.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        randomSpot = Random.Range(1, moveSpots.Length);

        //dazedTime = startDazedTime;
        anim = GetComponent<Animator>();
        anim.SetBool("isRunning", true);
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position/*moveSpot.position*/, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, moveSpots[randomSpot].position/*moveSpot.position*/) < 0.2f)
        {
            if (waitTime <= 0/*Time.time >= waitTime*/) {
                //moveSpot.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
                randomSpot = Random.Range(0, moveSpots.Length);
                //waitTime = Time.time + 1f / startWaitTime;
                waitTime = startWaitTime;
            } else {
                waitTime -= Time.deltaTime;
            }
        }

        //if (dazedTime <= 0) {
        //    speed = 2f;
        //} else {
        //    speed = 0;
        //    dazedTime -= Time.deltaTime;
        //}

        if (speed == 0) {
            anim.SetBool("isRunning", false);
        } else {
            anim.SetBool("isRunning", true);
        }
    }

    void FixedUpdate()
    {
        //вхідні дані про переміщення вліво-вправо
        anim.SetFloat("move", Mathf.Abs(speed));
    }

    //public void WTF()
    //{
    //    dazedTime = startDazedTime;
    //}
}
