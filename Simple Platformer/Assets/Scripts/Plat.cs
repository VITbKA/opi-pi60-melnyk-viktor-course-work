﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plat : MonoBehaviour
{
    private Transform player;
   // private Animator anim;

    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask whatIsGround;

    //public Transform p;
    //public GameObject plat;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //p = GameObject.FindGameObjectWithTag("p").transform;
        //plat = GameObject.FindGameObjectWithTag("p");
        //anim = GetComponent<Animator>();
    }

    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && isGrounded == true)
        {
            //PlatAnim();
            //if (other.GetComponent<Plat>().plat)
            //    anim.SetTrigger("drug");
            Destroy(gameObject, 1f);
            Debug.Log("plat dastroyed!");
            this.enabled = false;
        }
    }

    //public void PlatAnim()
    //{

        
    //}
}
