﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPatrol : MonoBehaviour
{
    public float speed = 2f;
    public float distance;

    private float waitTime;
    public float startWaitTime;

    public Transform groundDetection;

    private Animator anim;
    private float dazedTime;
    public float startDazedTime = 1.2f;

    private bool movingRight = true;


    void Start()
    {
        waitTime = startWaitTime;

        dazedTime = startDazedTime;
        anim = GetComponent<Animator>();
        anim.SetBool("isRunning", true);
    }

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        //if (groundInfo.collider == false)
        //{
        //    if (movingRight == true)
        //    {
        //        transform.eulerAngles = new Vector3(0, 0, 0);
        //        movingRight = false;
        //    }
        //    else
        //    {
        //        transform.eulerAngles = new Vector3(0, -180, 0);

        //        movingRight = true;
        //    }
        //}
        if (Time.time >= waitTime && groundInfo.collider == false)
        {
            if (movingRight == true) {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = false;
                //ac.Attack();
            }
            else {
                transform.eulerAngles = new Vector3(0, 180, 0);
                movingRight = true;
                //ac.Attack();
            }

            waitTime = Time.time + 1f / startWaitTime;
        } else {
            waitTime -= Time.deltaTime;
        }

        // Dazed
        if (dazedTime <= 0) {
            speed = 2f;
        } else if (dazedTime >= 0) {
            speed = 0;
            dazedTime -= Time.deltaTime;
        }

        // Anim run
        if (speed == 0) {
            anim.SetBool("isRunning", false);
        } else {
            anim.SetBool("isRunning", true);
        }
    }

    void FixedUpdate()
    {
        //вхідні дані про переміщення вліво-вправо
        anim.SetFloat("move", Mathf.Abs(speed));
    }

    public void WTF()
    {
        dazedTime = startDazedTime;
    }
}
