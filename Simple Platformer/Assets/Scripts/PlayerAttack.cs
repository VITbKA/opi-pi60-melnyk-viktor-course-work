﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private float timeBtwAttack = 0f;
    public float startTimeBtwAttack = 1f;

    public Transform attackPos;
    public float attackRange = 0.6f;
    public LayerMask whatIsEnemies;
    public int damage = 1;

    public Animator anim;

    void Update()
    {
        if (Time.time >= timeBtwAttack)
        {
            // then u attack
            if (Input.GetMouseButtonDown(0) /*&& !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack")*/)
            {
                Attack();
                timeBtwAttack = Time.time + 1f / startTimeBtwAttack;
            }
        } else {
            timeBtwAttack -= Time.deltaTime;
        }
    }


    void Attack()
    {

        // Play an attack animation
        // anim.SetTrigger("attack");
        //FindObjectOfType<AudioManager>().Play("AttackPlayer");

        // Detect enemies in range of attack
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);

        // Damage them
        foreach (Collider2D enemy in enemiesToDamage)
        {
            enemy.GetComponent<Enemy>().TakeDamage(damage);
        }
    }


    void onDrawGizmosSelected()
    {
        if (attackPos == null)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
