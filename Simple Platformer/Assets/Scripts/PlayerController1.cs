﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController1: MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D rb;

    public float speed, jumpForce;
    private float moveInput;

    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask whatIsGround;

    private float jumpTimeCouner;
    public float jumpTime;

    private bool isJumping;
    //private bool facingRight = true;

    public int extraJumps;
    public int extraJumpsValue;

    private bool isCrouch;
    private bool attacking;

    public Sound s;

    void Start()
    {
        extraJumps = extraJumpsValue;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

        // Flip
        if (moveInput > 0) {
            transform.eulerAngles = new Vector3(0, 0, 0);
        } else if (moveInput < 0) {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        // На земле - екстра джамп обновляется (jump)
        if (isGrounded == true) {
            extraJumps = extraJumpsValue;
            anim.SetBool("isJumping", false);
        }
        else {
            anim.SetBool("isJumping", true);
        }

        // double jump
        if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("takeOf");

            isJumping = true;
            FindObjectOfType<AudioManager>().Play("PlayerJump");
            jumpTimeCouner = jumpTime;
            rb.velocity = Vector2.up * jumpForce;
        } else if (Input.GetKeyDown(KeyCode.Space) && extraJumps > 0) {
            isJumping = true;
            FindObjectOfType<AudioManager>().Play("PlayerJump2");
            jumpTimeCouner = jumpTime;
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        } 

        if (Input.GetKey(KeyCode.Space) && isJumping == true)
        {
            if (jumpTimeCouner > 0) {
                anim.SetBool("isJumping", true);
                
                rb.velocity = Vector2.up * jumpForce;
                jumpTimeCouner -= Time.deltaTime;
            }
            else {
                isJumping = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }

        if (moveInput == 0) {
            anim.SetBool("isRunning", false);
            FindObjectOfType<AudioManager>().Close("PlayerMove");
        }
        else //if (moveInput > 0.0 && (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D)))
        {
            anim.SetBool("isRunning", true);
            if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) && isGrounded == true)
            {
                FindObjectOfType<AudioManager>().Play("PlayerMove");
            }
        }

        
        if (isGrounded == true && Input.GetKeyDown(KeyCode.S)) {
            anim.SetBool("isSit", true);
            speed = 2;
        } else if(isGrounded == true && Input.GetKeyUp(KeyCode.S)) {
            anim.SetBool("isSit", false);
            speed = 5;
        }

        //HandleInput();
        //анімація ударів
        if (moveInput == 0 && Input.GetMouseButtonDown(0) && isGrounded == true /*&& !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack")*/)
        {
            anim.SetTrigger("attack");
            rb.velocity = Vector2.zero;
        }
        else if (speed > 0 && Input.GetMouseButtonDown(0) && isGrounded == true && !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack in Run"))
        {
            anim.SetTrigger("attack 2");
        } 
    }

    void FixedUpdate()
    {
        moveInput = Input.GetAxisRaw("Horizontal");

        //вхідні дані про переміщення вліво-вправо
        anim.SetFloat("move", Mathf.Abs(moveInput));

        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        //HandleAttack();
        //ResetValue();

        /*if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        }*/
    }

    /*void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }*/

    void HandleAttack()
    {
        if (attacking)
        {
            anim.SetTrigger("attack");
        }
    }

    void HandleInput()
    {
        if (Input.GetMouseButtonDown(0) && isGrounded == true && isJumping == false)
            attacking = true;
    }

    void ResetValue()
    {
        attacking = false;
    }
}
