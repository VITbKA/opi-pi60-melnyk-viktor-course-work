﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int health = 8;
    public int numOfHearts;

    public UnityEngine.UI.Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    private Animator anim;
    public GameObject bloodEffect;
    private Rigidbody2D rb;

    private bool isJumping;

    [SerializeField]
    private GameObject gameOverUI;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (health > numOfHearts) {
            health = numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health) { hearts[i].sprite = fullHeart; }
            else { hearts[i].sprite = emptyHeart; }

            if (i < numOfHearts) { hearts[i].enabled = true; }
            else { hearts[i].enabled = false; }

        }
    }

    public void Hurt(int damage)
    {
        anim.SetTrigger("Hurt");
        health -= damage;
        

        FindObjectOfType<AudioManager>().Play("NinjaHit");

        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        FindObjectOfType<AudioManager>().Close("PlayerMove");
        FindObjectOfType<AudioManager>().Play("PlayerDeth");
        anim.SetBool("isDead", true);

        Instantiate(bloodEffect, transform.position, Quaternion.identity);
        Debug.Log("Player DIE");
        gameOverUI.SetActive(true);

        //GetComponent<Enemy_attack>().enabled = false;
        //GetComponent<PlayerHealth>().GetComponent<Enemy_attack>().enabled = false;

        GetComponent<CapsuleCollider2D>().enabled = false;
        GetComponent<PlayerController1>().enabled = false;
        GetComponent<PlayerAttack>().enabled = false;
    }

    public IEnumerator KnockBack(float knockDur, float knockbackPwr, Vector3 knockbackDir)
    {
        float timer = 0;

        while (knockDur > timer)
        {
            timer += Time.deltaTime;
            rb.AddForce(new Vector3(knockbackDir.x * 50, knockbackDir.y * knockbackPwr, transform.position.z));
        }

        yield return 0;
    }
}
