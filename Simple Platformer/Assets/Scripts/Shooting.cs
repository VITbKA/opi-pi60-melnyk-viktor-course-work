﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    //public Transform firePoint;
    public GameObject bulletPrefab;

    public float speed;
    public float stoppingDistance = 5f;
    public float retreatDistance = 1f;

    
    private Transform target;
    

    //public float bulletForce = 20f;
    private float timeBtwShots = 0f;
    public float startBtwShots = 0.3f;
    

    public Animator anim;

    void Start() {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        //timeBtwShots = Time.deltaTime;
    }

    void Update() {
        if (Vector2.Distance(transform.position, target.position) > stoppingDistance) {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        } else if (Vector2.Distance(transform.position, target.position) < stoppingDistance && Vector2.Distance(transform.position, target.position) > retreatDistance)
        {
            transform.position = this.transform.position;
        } else if (Vector2.Distance(transform.position, target.position) < retreatDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, -speed * Time.deltaTime);
        }

        if (Time.time >= timeBtwShots) {
            Shoot();
            //Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            timeBtwShots = Time.time + 1f / startBtwShots;
        } else {
            timeBtwShots -= Time.deltaTime;
        }
    }

    void Shoot() {
        // shooting logic

        //RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.right);
        //if (hitInfo)
        //{
        //    Debug.Log(hitInfo.transform.name);
        //}
        Debug.Log("HIT");

        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
    }
}
