﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    public int damage = 1;
    public GameObject hitEffect;

    private Transform player;
    //private PlayerHealth ph;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //ph = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<AudioManager>().Play("SpikeHit");

            StartCoroutine(other.GetComponent<PlayerHealth>().KnockBack(0.076f, 1, player.transform.position));
            // Damage player
            //other.GetComponent<Enemy>().TakeDamage(damage);
            // other.GetComponent<PlayerHealth>().health -= damage;
            other.GetComponent<PlayerHealth>().GetComponent<Animator>();
            other.GetComponent<PlayerHealth>().Hurt(damage);
            Debug.Log(other.GetComponent<PlayerHealth>().health);

            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
            Debug.Log("HIT PLAYER");
        }
    }
}
